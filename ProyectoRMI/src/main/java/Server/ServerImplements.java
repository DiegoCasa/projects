/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import RMI.RemoteInterface;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author diegocasa
 */
public class ServerImplements extends UnicastRemoteObject implements RemoteInterface{
    public ServerImplements() throws Exception{
        super();    
    }

    @Override
    public int suma(int a, int b) {
        return (a+b);
    }

    @Override
    public int resta(int a, int b) throws Exception {
        return (a-b);
    }

    @Override
    public int multiplica(int a, int b) throws Exception {
        return (a*b);
    }

    @Override
    public int division(int a, int b) throws Exception {
        return (a/b);
    }
}
