/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;

import java.rmi.Remote;

/**
 *
 * @author diegocasa
 */
public interface RemoteInterface extends Remote{
    public int suma(int a, int b) throws Exception;
    public int resta(int a, int b) throws Exception;
    public int multiplica(int a, int b) throws Exception;
    public int division(int a, int b) throws Exception;
}
