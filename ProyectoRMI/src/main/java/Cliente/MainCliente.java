/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import RMI.RemoteInterface;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.JOptionPane;

/**
 *
 * @author diegocasa
 */
public class MainCliente {
    
    public static void main(String[] args) {
        try{
            String valora = JOptionPane.showInputDialog("Ingrese numero 1");
            String valorb = JOptionPane.showInputDialog("Ingrese numero 2");
            int a = Integer.parseInt(valora);
            int b = Integer.parseInt(valorb);
            Registry miRegistro = LocateRegistry.getRegistry("127.0.0.1", 1234);
            RemoteInterface s = (RemoteInterface) miRegistro.lookup("Matematicas");
            JOptionPane.showMessageDialog(null,"Resultados suma: "+s.suma(a,b));
            JOptionPane.showMessageDialog(null,"Resultados resta: "+s.resta(a,b));
            JOptionPane.showMessageDialog(null,"Resultados multiplicacion: "+s.multiplica(a,b));
            JOptionPane.showMessageDialog(null,"Resultados division: "+s.division(a,b));
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
    }
    
}
