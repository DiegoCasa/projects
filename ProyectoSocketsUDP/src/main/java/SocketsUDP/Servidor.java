/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketsUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author diegocasa
 */
public class Servidor {
    
    public static void main(String[] args) {
        
        try{
            final int PUERTO = 5000;
            byte[] buffer = new byte[1024];
            
            System.out.println("Iniciado el servidor");
            
            while(true){
                DatagramSocket socketUDP = new DatagramSocket(PUERTO);
                
                DatagramPacket peticion = new DatagramPacket(buffer, buffer.length);
                socketUDP.receive(peticion);
                String mensaje = new String(peticion.getData());
                System.out.println(mensaje);
                
                int puertoCliente = peticion.getPort();
                InetAddress direccion = peticion.getAddress();
                
                mensaje = "saludo desde servidor";
                buffer = mensaje.getBytes();
                
                DatagramPacket respuesta = new DatagramPacket(buffer, buffer.length, direccion, puertoCliente);
                socketUDP.send(respuesta);
            }
            
        } catch (SocketException ex){
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex){
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
