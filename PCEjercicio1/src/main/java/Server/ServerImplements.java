/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import RMI.RemoteInterface;
import java.rmi.server.UnicastRemoteObject;
/**
 *
 * @author diegocasa
 */
public class ServerImplements extends UnicastRemoteObject implements RemoteInterface{
    public ServerImplements() throws Exception{
        super();    
    }

    @Override
    public String pares(int a, int b) {
        String result = "";
        for (int i = a; i <= b; i++) {
            //valida numeros pares
            if (i % 2 == 0) {
                result = result + " , " + i;
            }
        }
        return result;
    }

    @Override
    public String primos(int a, int b) throws Exception {
        String result = "";
        for (int x=a;x<=b;x++) {
            //Inicia valida numeros primos
            int contador = 2;
            boolean primo=true;
            while ((primo) && (contador!=x)){
              if (x % contador == 0)
                primo = false;
              contador++;
            }
            //verifica si es numero primo
            if (primo){
                result = result + " , " + x;
            }			
        }
        return result;
    }
    
}
