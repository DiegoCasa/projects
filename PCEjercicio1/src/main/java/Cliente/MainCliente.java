/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import RMI.RemoteInterface;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.JOptionPane;

/**
 *
 * @author diegocasa
 */
public class MainCliente {
    public static void main(String[] args) {
        try{
            String valora = JOptionPane.showInputDialog("Ingrese Inferior");
            String valorb = JOptionPane.showInputDialog("Ingrese Superior");
            int a = Integer.parseInt(valora);
            int b = Integer.parseInt(valorb);
            Registry miRegistro = LocateRegistry.getRegistry("127.0.0.1", 5678);
            RemoteInterface s = (RemoteInterface) miRegistro.lookup("Practica");
            JOptionPane.showMessageDialog(null,"Resultados pares: "+s.pares(a,b));
            JOptionPane.showMessageDialog(null,"Resultados primos: "+s.primos(a,b));
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
