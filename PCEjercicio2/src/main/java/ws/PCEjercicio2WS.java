/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author diegocasa
 */
@WebService(serviceName = "PCEjercicio2WS")
public class PCEjercicio2WS {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "area")
    public Double area(@WebParam(name = "a") double a) {
        //TODO write your implementation code here:
        //Se trabajo con libreria MAth
        return Math.PI * Math.pow(a,2);
    }
    
}
